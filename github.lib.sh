#!/bin/bash
# Requires the following environment variables:
# GITHUB_ORGANIZATION
# WORK_DIRECTORY
# Requires the following functions
# set-progress-bar-percentage

function pr() {
	local message=$*
	local branch="${message// /_}"
	git checkout -b $branch
	git commit -am "$message"
	git push -u origin $branch
	gh pr create -f
}

function for-each-repo() {
	# todo: it would be nice if we could have a skip list, and a way to continue from some repo
	local current_directory=$(pwd)
	local script=$*
	local repos=$(gh repo list $GITHUB_ORGANIZATION --no-archived --limit 250 --json name --jq ".[].name" | sort)
	local number_of_repos=$(echo "$repos" | wc -w)
	local number_of_repos_processed=0
	#echo "Processing $number_of_repos repos"
	for repo in $repos
	do
		#echo "Processing $repo - $script"
		eval "${script}"
		number_of_repos_processed=$((number_of_repos_processed+1))
		set-progress-bar-percentage $number_of_repos_processed $number_of_repos
	done
	cd "$current_directory"
}

function mass_update_master() {
	for-each-repo 'cd "$WORK_DIRECTORY/$repo" && (git checkout master || git checkout main) && git pull'
}

function failing-build-links() {
	local repo=$1
	gh run list -b $(gh api repos/sesamecare/$repo --jq '.default_branch') --limit 1 --repo sesamecare/$repo --json status,url,displayTitle,conclusion | jq -r 'if (.[].conclusion == "failure") then (.[].url + " " + .[].displayTitle) else empty end'
}

function list-failing-builds() {
	for-each-repo 'failing-build-links $repo'
}

function mass_approve_review_on_pr() {
	local search_string="$1"
	for-each-repo 'approve_review_on_pr "$search_string" $repo'
}

function approve_review_on_pr() {
	# run with: (\ to un-alias ls)
	# must do "export -f approve_review_on_pr" first to have xargs work with bash functions
	# \ls | xargs -I {} bash -c 'approve_review_on_pr MEG_636 {}'
	local search_string="$1"
	local repo=$2
	echo "Checking for PRs containing $search_string in $GITHUB_ORGANIZATION/$repo"
	gh --repo $GITHUB_ORGANIZATION/$repo pr list | grep "$search_string" | cut -f1 | xargs -I  {} gh --repo $GITHUB_ORGANIZATION/$repo pr review {} --approve
}
function mass_merge_prs() {
	local search_string="$1"
	for-each-repo 'merge_pr "$search_string" "$repo"'
}
function merge_pr() {
	# run with: (\ to un-alias ls)
	# must do "export -f approve_review_on_pr" first to have xargs work with bash functions
	# \ls | xargs -I {} bash -c 'approve_review_on_pr MEG_636 {}'
	local search_string="$1"
	local repo=$2
	echo "Checking for PRs containing \"$search_string\" in $GITHUB_ORGANIZATION/$repo"
	gh --repo $GITHUB_ORGANIZATION/$repo pr list | grep "$search_string" | cut -f1 | xargs -I  {} gh --repo $GITHUB_ORGANIZATION/$repo pr merge {} --squash
}

function mass_clone_new_repos() {
	for-each-repo 'if [ ! -e "$WORK_DIRECTORY/$repo" ]; then git clone -c core.autocrlf=false -c core.filemode=false https://github.com/$GITHUB_ORGANIZATION/$repo.git $WORK_DIRECTORY/$repo; fi'
	#grep -v -F -f <(\ls | sort) <(gh repo list $GITHUB_ORGANIZATION --no-archived --limit 250 --json name --jq ".[].name" | sort)  | xargs -n1 --verbose -I {} git clone -c core.autocrlf=false -c core.filemode=false https://github.com/$GITHUB_ORGANIZATION/{}.git
}
