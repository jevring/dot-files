These are my "dot-files", i.e. everything I need for my environment.
I'm using cygwin/bash, as well as Console2.

To launch Console2, create a shortcut to the following: 
`C:\tools\Console2\Console.exe -c %USERPROFILE%\Documents\dot-files\console.xml`

To install, do:
`cd && git clone https://bitbucket.org/jevring/dot-files.git && dot-files/install.sh`
