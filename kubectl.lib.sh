function pods() {
	kubectl get pods --all-namespaces | grep -v kube | grep -v default
}
function wpods() {
	watch "kubectl get pods --all-namespaces --sort-by=metadata.creationTimestamp | grep -v kube | grep -v default"
}
function pod-name() {
	local namespace=$1
	local prefix=$2
	# Unfortunately this doesn't work
	# | sort -k4 -h -r 
	kubectl get pods -n $namespace | grep $prefix | cut -d" " -f 1
}
function kubectl-logs() {
	local namespace=$1
	local prefix=$2
	kubectl logs -f -n $namespace $3 $(pod-name $namespace $prefix)
}

function kubectlgetall {
  for i in $(kubectl api-resources --verbs=list --namespaced -o name | grep -v "events.events.k8s.io" | grep -v "events" | sort | uniq); do
    echo "Resource:" $i
    
    if [ -z "$1" ]
    then
        kubectl get --ignore-not-found ${i}
    else
        kubectl -n ${1} get --ignore-not-found ${i}
    fi
  done
}
