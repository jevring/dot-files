:set nocompatible
:syntax on
:set ruler
:set nowrap
:set autoindent
:set smartindent
:set showmatch
:set ignorecase "Ignore case when searching
:set smartcase
:set incsearch "Make search act like search in modern browsers
:set nolazyredraw "Don't redraw while executing macros
:set background=dark " we plan to use a dark background
:set backspace=indent,eol,start " make backspace a more flexible
:set scrolloff=5 " Keep 10 lines (top/bottom) for scope
:set ttyfast
" todo: this needs some work, since the "Console" in the title overrides the first N characters
set title "Set terminal title to filename


:set laststatus=2
:set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]
 "              | | | | |  |   |      |  |     |    |
 "              | | | | |  |   |      |  |     |    + current
 "              | | | | |  |   |      |  |     |       column
 "              | | | | |  |   |      |  |     +-- current line
 "              | | | | |  |   |      |  +-- current % into file
 "              | | | | |  |   |      +-- current syntax in
 "              | | | | |  |   |          square brackets
 "              | | | | |  |   +-- current fileformat
 "              | | | | |  +-- number of lines
 "              | | | | +-- preview flag in square brackets
 "              | | | +-- help flag in square brackets
 "              | | +-- readonly flag in square brackets
 "              | +-- rodified flag in square brackets
 "              +-- full path to file in the buffer

" http://vim.wikia.com/wiki/Folding
" http://vim.wikia.com/wiki/Vim_as_XML_Editor
let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax
au FileType xsd setlocal foldmethod=syntax
set foldmarker={,}
set foldmethod=marker
set foldtext=substitute(getline(v:foldstart),'{.*','{...}',)
" set this to see a fold outline on the left side
"set foldcolumn=4
set foldlevelstart=100
set foldlevel=1

imap <kMinus> zc
nmap <kMinus> zc
omap <kMinus> zc
vmap <kMinus> zc

" TODO: map ctrl+home and ctrl+end


" TODO: What's the difference between 'nnoremap' and 'noremap'?
" Next Tab
" TODO: Doesn't work
nnoremap <silent> <C-PageUp> :tabnext<CR>
nnoremap <silent> <C-PageDn> :tabprevious<CR>
nnoremap <silent> <C-t> :tabnew<CR>

" ***************************
" * keyboard mapping ********
" ***************************
"imap <A-1> <Esc>:tabn 1<CR>i
"map <A-Left> :tabprevious<CR>
"map <A-Right> :tabnext<CR>


" save me from constant typos, since SHIFT is held to type the :
:command WQ wq
:command Wq wq
:command W w
:command Q q


" TODO: what does this do?
:set wildmenu
:set wildmode=list:longest,full

" this enables mouse selection, which we almost never want
" :set mouse=a
