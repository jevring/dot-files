export DOTFILES_HOME=~/dot-files

# Let's use resolve the latest version of a dir automatically
function latestVersionDirectory() {
	echo $1$(ls -vr "$1" | grep $2 | head -n 1)
}
export M2_HOME=/opt/apache-maven-3.9.2
export M2=$M2_HOME/bin
export MAVEN_OPTS=-Xmx512M
export GRADLE_USER_HOME=~/.gradle
#export JAVA_HOME=/opt/jdk-19.0.2
#export JAVA_HOME=/opt/jdk-20.0.1
export JAVA_HOME=/opt/jdk-21
export GOROOT=~/bin/go

# Add the bin dirs of the various home dirs to the path
export PATH=$PATH:$M2
export PATH=$PATH:$GRADLE_HOME/bin
export PATH=$PATH:$GCLOUD_HOME/bin
# pip install:
export PATH=$PATH:~/.local/bin
export PATH=$PATH:"~/bin/"
# See config in .npmrc
# This is especially needed for bash-on-windows
export PATH=$PATH:/opt/npm/.packages/bin
export PATH=$PATH:$JAVA_HOME/bin

export PATH=$PATH:$GOROOT/bin


function extract-video-from-motion-photo() {
	local offset=$(exiftool -MicroVideoOffset -s3 $1)
	local length=$(stat -c'%s' $1)
	local bs=$(expr $length - $offset)
	dd if=$1 of=$(basename $1 .jpg).mp4 bs=$bs skip=1
}
export PATH=$PATH:/usr/local/go/bin

# krew is a kubectl plugin manager
# https://github.com/kubernetes-sigs/krew
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

function git-clean-old-branches() {
	git fetch --prune
	git checkout master
	# Pulling is probably nice, but it'll likely slow the command down
	# git pull
	# From https://stackoverflow.com/a/49518495
	git branch --list --format   "%(if:equals=[gone])%(upstream:track)%(then)%(refname:short)%(end)" | xargs -r git branch -D
}

function hard-kill-namespace() {
	# Apparently we can do this instead of the proxy stuff:
	#kubectl patch configmap/mymap \
    #--type json \
    #--patch='[ { "op": "remove", "path": "/metadata/finalizers" } ]'
	#https://kubernetes.io/blog/2021/05/14/using-finalizers-to-control-deletion/
	local namespace=$1
	kubectl proxy &
	PID=$!
	sleep 5
	kubectl get ns $namespace -o json | jq '.spec.finalizers=[]' | curl -X PUT http://localhost:8001/api/v1/namespaces/$namespace/finalize -H "Content-Type: application/json" --data @-
	kill $PID
	kubectl delete ns $namespace
}


function hl() {
	#https://stackoverflow.com/questions/981601/colorized-grep-viewing-the-entire-file-with-highlighted-matches
	grep --color "$1\|$" $2
}
function local-docker() {
	# test-containers requires this to be localhost, rather than 0.0.0.0
	export DOCKER_HOST=tcp://localhost:2375
	#export DOCKER_HOST=tcp://0.0.0.0:2375
}
function video2gif() {
  #ffmpeg -y -i "${1}" -vf fps=${3:-10},scale=${2:-320}:-1:flags=lanczos,palettegen "${1}.png"
  local palette="${1}.png"
  ffmpeg -i "${1}" -vf palettegen -y $palette
  ffmpeg -i "${1}" -i $palette -lavfi "paletteuse" -y "${1}".gif
  rm $palette
}
function youtube-dl-chapters-to-cue() {
	java -jar ~/code/Java/youtube-dl-chapters-to-cue/target/youtube-dl-chapters-to-cue-1.0-SNAPSHOT-jar-with-dependencies.jar "$1"
}
function youtube-mp3() {
	yt-dlp -x --audio-format mp3 --audio-quality 0 --embed-thumbnail --add-metadata --write-info-json -o '%(title)s/%(title)s-%(id)s.%(ext)s' $*
}
function youtube-mp3-old() {
	local dir=$(youtube-dl --get-filename -o '%(title)s' $*)
	youtube-dl -x --audio-format mp3 --audio-quality 0 --embed-thumbnail --add-metadata --write-info-json -o '%(title)s/%(title)s-%(id)s.%(ext)s' $*
	cd "$dir"
	youtube-dl-chapters-to-cue "$(find | grep .info.json)"
	#rm "$(find | grep .info.json)"
	cd ..
}
function youtube-mp3-playlist() {
	# If we don't do sort+uniq we'll get a name for every single file in the list
	local dir=$(yt-dlp --get-filename -o '%(artist)s - %(playlist)s' $* | sort | uniq)
	yt-dlp -x --audio-format mp3 --audio-quality 0 --embed-thumbnail --add-metadata --write-info-json -o '%(artist)s - %(playlist)s/%(playlist_index)s - %(title)s-%(id)s.%(ext)s' $*
	cd "$dir"
	ls *.mp3 > playlist.m3u
	cd ..
}
function youtube-mp3-playlist-old() {
	# If we don't do sort+uniq we'll get a name for every single file in the list
	local dir=$(youtube-dl --get-filename -o '%(artist)s - %(playlist)s' $* | sort | uniq)
	youtube-dl -x --audio-format mp3 --audio-quality 0 --embed-thumbnail --add-metadata --write-info-json -o '%(artist)s - %(playlist)s/%(playlist_index)s - %(title)s-%(id)s.%(ext)s' $*
	cd "$dir"
	ls *.mp3 > playlist.m3u
	cd ..
}

function youtube-1080() {
	# Limit to 1080p. This is sometimes needed to avoid smearing in "original" resolution
	yt-dlp -f "bestvideo[height<=1080]+bestaudio" $*
}
function youtube-1080-old() {
	# Limit to 1080p. This is sometimes needed to avoid smearing in "original" resolution
	youtube-dl -f "bestvideo[height<=1080]+bestaudio" $*
}
function it() {
	mvn clean install -Pintegration-test-normal $*
#	&& flash
}
function mci() {
	mvn clean install $*
#	&& flash
}
function mcd() {
	mvn clean deploy $* 
#	&& flash
}
function fmcit() {
	mvn clean install -Dsurefire.useFile=false -Dmaven.test.skip -Dcheckstyle.skip=true -Dfindbugs.skip=true -Djacoco.skip=true -Dmaven.javadoc.skip=true -Dmaven.source.skip=true $*
#	&& flash	
}
function it() {
	mci -Pintegration-test-normal
}
function start() {
	# mimic the "start" command in windows. normally it runs whatever is passed
	# according to the mime type, but 99% of usage is to launch explorer windows
	explorer.exe `winpath $1`
}
function winpath() {
	# From: https://gist.github.com/JacobDB/e71e22c3663175b2369211aeb54362fe


    # get the Windows user path, convert to Unix line endings
    user_path=$(echo "$(/mnt/c/Windows/System32/cmd.exe /C echo %HOMEDRIVE%%HOMEPATH%)" | tr -d "\r")

    # expand the specified path
    target_path=$(readlink -f $1)

    # change ~ to $user_path
    if grep -q "^/home/" <<< $target_path; then
        temp_user_path=$(echo "$user_path" | sed -e 's|\\|/|g' -e 's|^\([A-Za-z]\)\:/\(.*\)|/mnt/\L\1\E/\2|' -e 's|^M$||')

        # if there was something after ~, add it to the end of the $user_path
        if grep -q "^/home/\(.*\)/\(.*\)" <<< $target_path; then
            target_path=$temp_user_path$(echo "$target_path" | sed -e 's|^/home/*/\(.*\)|/\2|')
        # if there was nothing after ~, $target_path is $user_path
        else
            target_path=$temp_user_path
        fi
    fi

    # check if a Windows path is getting parsed
    if grep -q "^/mnt/[a-z]" <<< $target_path; then
        # swap /mnt/[a-z] with [A-Z]: and / with \
        echo $(echo "$target_path" | sed -e 's|^\(/mnt/\([a-z]\)\)\(.*\)|\U\2:\E\3|' -e 's|/|\\|g')
    else
        # return the user's home directory if a Unix path was parsed
        echo $user_path
    fi
}
function sloc() {
	cloc --exclude-dir=.idea,target,.gradle,build .
	#cloc --exclude-dir=.idea,target .
	#cloc --exclude-dir=.idea,target,mocks .
}
function mdr() { 
	# render markdown in the shell
	pandoc $1 | lynx -stdin -dump 
} 
function docker-ip() {
	docker inspect --format '{{ .NetworkSettings.IPAddress }}' $@
}

alias ls="ls --color"
alias jvisualvm="jvisualvm --console suppress"
alias n="\"$BASE_DIR/Program Files (x86)/Notepad++/notepad++\""
alias grep='grep --color=auto'

function ps1-on() {
	# In some repositories, having these flags on makes the prompt *super slow*.
	# As a result, we'll need to be able to turn these features on and off at will

	# load __git_ps "magic" keywords
	export GIT_PS1_SHOWDIRTYSTATE=1
	export GIT_PS1_SHOWSTASHSTATE=1
	#export GIT_PS1_SHOWUNTRACKEDFILES=1
	# Explicitly unset color (default anyhow). Use 1 to set it.
	# export GIT_PS1_SHOWCOLORHINTS=1
	# export GIT_PS1_DESCRIBE_STYLE="branch"
	export GIT_PS1_SHOWUPSTREAM="auto git"
}

function ps1-off() {
	unset GIT_PS1_SHOWDIRTYSTATE
	unset GIT_PS1_SHOWSTASHSTATE
	unset GIT_PS1_SHOWUPSTREAM
}

### load ssh-agent if it's not running
### from http://superuser.com/questions/441854/git-ssh-agent-not-working
### The github page about this was worse than useless =(
function agent_running() {
    [ "$SSH_AUTH_SOCK" ] && { ssh-add -l >/dev/null 2>&1 || [ $? -eq 1 ]; }
}
function run_ssh_agent_if_not_already_running() {
	# Ensure that the directory already exists
	mkdir -p ~/.ssh
	env=~/.ssh/agent.env

	if ! agent_running && [ -s "$env" ]; then
		. "$env" >/dev/null
	fi

	if ! agent_running; then
		echo "Running ssh agent"
		ssh-agent >"$env"
		. "$env" >/dev/null
		ssh-add ~/.ssh/id_ed25519
		#ssh-add ~/.ssh/google_compute_engine
	fi

	unset env
}
# Uncomment this if we actually want the ssh agent running
#run_ssh_agent_if_not_already_running
### end ssh-agent

## Configure history
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
export HISTCONTROL=ignoreboth
shopt -s histappend
export HISTFILESIZE=
export HISTSIZE=

## Timestamp for when a command was EXECUTED in the prompt
# https://redandblack.io/blog/2020/bash-prompt-with-updating-time/
NEWLINE="\n"
NO_COLOUR="\e[00m"
PRINTING_OFF="\["
PRINTING_ON="\]"
PROMPT_COLOUR="\e[0;33m"
PS1_PROMPT="\$"
PS2_PROMPT=">"
RESTORE_CURSOR_POSITION="\e[u"
SAVE_CURSOR_POSITION="\e[s"
SINGLE_SPACE=" "
TIMESTAMP="\t"
TIMESTAMP_PLACEHOLDER="--:--:--"

move_cursor_to_start_of_ps1() {
    command_rows=$(history 1 | wc -l)
    if [ "$command_rows" -gt 1 ]; then
        let vertical_movement=$command_rows+1
    else
        command=$(history 1 | sed 's/^\s*[0-9]*\s*//')
        command_length=${#command}
        ps1_prompt_length=${#PS1_PROMPT}
        let total_length=$command_length+$ps1_prompt_length
        let lines=$total_length/${COLUMNS}+1
        let vertical_movement=$lines+1
    fi
    tput cuu $vertical_movement
}

PS0_ELEMENTS=(
    "$SAVE_CURSOR_POSITION" "\$(move_cursor_to_start_of_ps1)"
    "$PROMPT_COLOUR" "$TIMESTAMP" "$NO_COLOUR" "$RESTORE_CURSOR_POSITION"
)
PS0=$(IFS=; echo "${PS0_ELEMENTS[*]}")

PS2_ELEMENTS=(
    "$PRINTING_OFF" "$PROMPT_COLOUR" "$PRINTING_ON" "$PS2_PROMPT"
    "$SINGLE_SPACE" "$PRINTING_OFF" "$NO_COLOUR" "$PRINTING_ON"
)
PS2=$(IFS=; echo "${PS2_ELEMENTS[*]}")

shopt -s histverify

## END Timestamp for when a command was EXECUTED in the prompt

function set-titlebar() {
	printf "\033]0;%s\007" "$*"
}
function set-progress-bar-percentage() {
	local progress=$1
	local total=$2
	local progress=$(echo "scale=2; ($progress / $total) * 100" | bc)
	render-progress-bar-value "$progress %"
}
function render-progress-bar-value() {
	# https://unix.stackexchange.com/questions/468574/making-a-progress-bar-in-bash-fixed-at-the-bottom-of-terminal
	local percentage=$1
	set-titlebar "Progress: $percentage"
#	tput sc # save current position
#	tput cup $((`tput lines`-1)) 0 # go to the bottom of the screen
	# Even moving to the top doesn't work, as the scrolling just erases it immediately.
	#tput cup 0 0 # top of the screen
	#tput el # erase line
	# It's "move cursor to $row $column"
	# todo: this gets overwritten if we are already on the last line
	# We should add a line break if we're on the last line. But then the next print line will move this UP one line
#	echo -n "Progress: $percentage"
#	tput rc
}
function get_dir() {
	# code below is MUCH faster
	# 59ms down to 0 ms for get_dir
    #printf "%s" $(pwd | sed "s:$HOME:~:")
	echo ${PWD/#$HOME/'~'}
}
function title() {
	# Allows you to set a window title prefix, to keep track of windows
	export TITLE="$1 "
}
function prompt_command() {
	# This must be the first thing, otherwise other commands' return codes might interfere
	local LAST_RETURN_CODE=$?
	# instantly add commands to history
	history -a
	local RED="\[\033[01;31m\]"
	local BLUE="\[\033[01;34m\]"
	local YELLOW="\[\033[01;33m\]"
	local GOLD="\e[0;33m"
	local RESET="\[\033[00m\]"

	# http://cwestblog.com/2013/01/29/bash-git-ps1-branch-based-coloring/
	# todo: __git_ps1 is very slow =(
	local git_branch=$(__git_ps1 "[git: %s]");
	# fasthgbranch comes from here:
	# http://sleepingcyb.org/2011/04/10/mercurial-current-branch-in-bash-prompt
	# I've included my own version in this repo with a slight change, 
	# as I didn't need to know the revision id.
	# If you want support for Mercurial, you're going to have to compile this yourself.
	# As such, I'm making the check optional
	local hg_branch;
	if [ -e ~/dot-files/fasthgbranch ]; then
		hg_branch=$(~/dot-files/fasthgbranch "[hg: %s]");
	fi
	local is_svn="";
	if [ -d .svn ]; then
		is_svn="[svn]"
	fi
	local k8s;
	if [ ! -z "$K8S_CLUSTER_OR_INSTANCE" ]; then
		k8s="[$K8S_CLUSTER_OR_INSTANCE] "
	fi
	# We could also use "kubectl config current-context" for this, if we wanted to
	local kubectx_context;
	if [ $(which kubectx) ]; then
		kubectx_context=$(kubectx -c)
		if [ "$kubectx_context" != "" ]; then
			k8s="[$kubectx_context] "
		fi
	fi
	local k8s_ip;
	if [ ! -z "$K8S_IP" ]; then
		k8s_ip="[$K8S_IP] "
	fi

	
	local branch=$git_branch$hg_branch$is_svn
	local dir=$(get_dir);
	local error_prompt_prefix;
	local error_title_prefix;
	if [ $LAST_RETURN_CODE != 0 ]; then
		error_prompt_prefix="$RED[Last return code: $LAST_RETURN_CODE]"
		error_title_prefix="ERROR! "
	fi
	PS1="$GOLD$TIMESTAMP_PLACEHOLDER $error_prompt_prefix$RED$k8s$k8s_ip$BLUE[$dir] $YELLOW$branch $RESET\n\\\$ ";
	
	# todo: this should include the running application name. Lead with it, even
	local titlebar_text;
	local dir_basename=$(basename "$dir");
	
	if [ "$dir_basename" != "$dir" ]; then
		# We're only interested in getting the basename if it won't be identical to the full name.
		# Granted, this likely only happens in places like ~ and /, but still
		titlebar_text="$TITLE$error_title_prefix$k8s$dir_basename $dir $branch";
	else
		titlebar_text="$TITLE$error_title_prefix$k8s$dir $branch"
	fi
	set-titlebar $titlebar_text
}
export PROMPT_COMMAND=prompt_command

# todo: enable again if we install docker
# eval $(docker-machine env --shell bash)

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi                                                                  

# unfuck the dircolors for directories
export LS_COLORS="ow=01;34;40"
export VISUAL=vim
export EDITOR="$VISUAL"

# Load rust/cargo
#source $HOME/.cargo/env

# Make ctrl+backspace delete a word
#bind '"\C-_":backward-kill-word'
# Windows Terminal sends ctrl+H, so that's what we need to map.
# I even personally put this in the comments in the ticket, and it works on one machine, but not on the other.
# https://github.com/microsoft/terminal/issues/755
#bind '"\C-H":backward-kill-word'
# This seems to work, though
stty werase ^H

# WSL2 can't handle DOCKER_HOST being set. It expects it to "just work", presumably via some pipes or something instead.
# WSL1 requires it, however
local-docker

for lib in ~/*.lib.sh; do
	if [ -e "$lib" ]; then
		echo "Loading $lib"
		source "$lib"
	fi
done
for lib in ~/dot-files/*.lib.sh; do
	if [ -e "$lib" ]; then
		echo "Loading $lib"
		source "$lib"
	fi
done

if [ -e ~/bash_history/backup.sh ]; then
	# Backup our .bash_history once a day
	# If it turns out that it's been wiped, we can use git to get it back.
	~/bash_history/backup.sh
else
	echo "Skipping bash_history backup"
fi
