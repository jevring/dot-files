#!/usr/bin/env bash
#
cd $HOME
ln -s dot-files/.vimrc .vimrc
ln -s dot-files/.bashrc .bashrc
ln -s dot-files/.npmrc .npmrc
